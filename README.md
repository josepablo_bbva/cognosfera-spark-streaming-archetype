Clone this bucket and install the archetype in your local maven repository with: mvn clean install

To create a new project from this archetype: 
mvn archetype:generate -DgroupId=com.bbva.kst.cognosphere -DartifactId=cognosfera-spark-streaming-XX -DarchetypeArtifactId=cognosfera-spark-streaming-archetype -DarchetypeGroupId=com.bbva.kst.cognosphere -DarchetypeVersion=1.0-SNAPSHOT -DinteractiveMode=false